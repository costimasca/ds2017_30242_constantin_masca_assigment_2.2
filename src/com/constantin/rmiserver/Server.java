package com.constantin.rmiserver;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

import com.constantin.common.CarInterface;

public class Server extends UnicastRemoteObject implements CarInterface {
	private static final long serialVersionUID = 1L;

	protected Server() throws RemoteException {
		super();
	}

	public static void main(String[] args) throws RemoteException {

		try{
			LocateRegistry.createRegistry(1099).list();
		}catch (RemoteException e) {
			System.out.println("rmiregistry was already running");
			LocateRegistry.getRegistry(1099).list();
		}

		try {
			Naming.rebind("//localhost/MyServer", new Server());
            System.err.println("Server ready");


        }catch (Exception e) {
        	System.err.println("Server exception: " + e.toString());
          e.printStackTrace();
        }
	}

	@Override
	public String helloTo(String name) throws RemoteException{
		System.err.println(name + " is trying to contact!");
		return "Server says hello to " + name;
	}

	@Override
	public Integer computeTax(Integer engineSize) throws RemoteException {
		int sum = 0;
		if(engineSize < 1600){
			sum = 8;
		} else if(engineSize < 2000) {
			sum = 18;
		}else if(engineSize < 2600) {
			sum = 72;
		}else if(engineSize < 3000) {
			sum = 144;
		}else {
			sum = 290;
		}
		return engineSize/200 * sum;
	}

	@Override
	public Integer computeSellPrice(Integer year, Integer price) throws RemoteException {
		return price - (price / 7) * (2015-year);
	}
}
