package com.constantin.rmiclient;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;

import com.constantin.common.CarInterface;

public class Client {
	private static CarInterface car;
	private static View view;

	public Client()throws MalformedURLException, RemoteException, NotBoundException  {
        view = new View();
        view.setVisible(true);
        view.addBtnPriceActionListener(new PriceButtonListener());
        view.addBtnTaxActionListener(new TaxButtonListener());

        car = (CarInterface) Naming.lookup("//localhost/MyServer");
    }


	class TaxButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                int engineSize = Integer.parseInt(view.getEngineSize());
                view.printTax(car.computeTax(engineSize));
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                displayErrorMessage("Please enter valid information!");
            }
        }
    }

    private class PriceButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
            int price = Integer.parseInt(view.getPrice());
            int year = Integer.parseInt(view.getYear());

                view.printPrice(car.computeSellPrice(year,price));

            }catch (RemoteException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                displayErrorMessage("Please enter valid information!");
            }
        }
    }

    private void displayErrorMessage(String s) {
	    view.clear();
	    JOptionPane.showMessageDialog(view,s,"Error",JOptionPane.ERROR_MESSAGE);
    }
}
