package com.constantin.rmiclient;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by constantin on 11/14/17.
 */
public class ClientStart {
    private ClientStart() {
    }

    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {
        new Client();
    }
}
