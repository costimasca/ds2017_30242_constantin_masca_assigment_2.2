package com.constantin.rmiclient;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	CatalogView is a JFrame which contains the UI elements of the Client application.
 */
public class View extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField textEngineSize;
    private JTextField textPrice;
    private JTextField textYear;
    private JButton btnPrice;
    private JButton btnTax;
    private JTextArea textArea;

    public View() {
        setTitle("HTTP Protocol simulator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblTax = new JLabel("Compute tax");
        lblTax.setBounds(10, 11, 120, 14);
        contentPane.add(lblTax);

        JLabel lblEngineSize = new JLabel("Engine Size");
        lblEngineSize.setBounds(10, 36, 100, 14);
        contentPane.add(lblEngineSize);

        textEngineSize = new JTextField();
        textEngineSize.setBounds(100, 33, 86, 20);
        contentPane.add(textEngineSize);
        textEngineSize.setColumns(10);


        btnTax = new JButton("Compute Tax");
        btnTax.setBounds(10, 90, 170, 23);
        contentPane.add(btnTax);

        JLabel lblSellingPrice = new JLabel("Compute Selling Price");
        lblSellingPrice.setBounds(255, 11, 170, 14);
        contentPane.add(lblSellingPrice);

        JLabel lblPrice = new JLabel("Price");
        lblPrice.setBounds(235, 36, 46, 14);
        contentPane.add(lblPrice);

        textPrice = new JTextField();
        textPrice.setBounds(320, 33, 86, 20);
        contentPane.add(textPrice);
        textPrice.setColumns(10);

        JLabel lblYear = new JLabel("Year");
        lblYear.setBounds(235, 61, 60, 14);
        contentPane.add(lblYear);

        textYear = new JTextField();
        textYear.setBounds(320, 61, 86, 20);
        contentPane.add(textYear);
        textYear.setColumns(10);

        btnPrice = new JButton("Compute Price");
        btnPrice.setBounds(235, 90, 170, 23);
        contentPane.add(btnPrice);

        textArea = new JTextArea();
        textArea.setBounds(25, 131, 400, 120);
        contentPane.add(textArea);

    }

    public void printTax(int tax) {
        textArea.setText("Tax: " + String.valueOf(tax));
    }

    public void printPrice(Integer price) {
        textArea.setText("Selling price: " + String.valueOf(price));
    }

    public void addBtnPriceActionListener(ActionListener e) {
        btnPrice.addActionListener(e);
    }

    public void addBtnTaxActionListener(ActionListener e) {
        btnTax.addActionListener(e);
    }

    public String getPrice() {
        return textPrice.getText();
    }

    public String getEngineSize() {
        return textEngineSize.getText();
    }

    public void clear() {
        textPrice.setText("");
        textEngineSize.setText("");
        textYear.setText("");
        textArea.setText("");
    }

    public String getYear() {
        return textYear.getText();
    }


}
