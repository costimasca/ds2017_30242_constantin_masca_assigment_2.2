package com.constantin.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CarInterface extends Remote {
	public String helloTo(String name) throws RemoteException;
	public Integer computeTax(Integer engineSize) throws RemoteException;
	public Integer computeSellPrice(Integer year, Integer price) throws RemoteException;
}